package Week4;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

//import org.openqa.selenium.By;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class Demo1 extends base {

	
	public static void main(String[] args) throws MalformedURLException {
		AndroidDriver<AndroidElement> driver = capabilities();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//ByXpath- //tag name (class name)[@attribute (text/resource-id) = 'value' (value='text value')]
		driver.findElementByXPath("//android.widget.Textview[@text='Preference']").click();
		driver.findElementByXPath("//android.widget.Textview[@text='3. Preference dependencies']").click();
		driver.findElementById("android:id/checkbox").click();
		//find Element using xpath with index
		driver.findElementByXPath("//android.widget.RelativeLayout[2]").click();
		driver.findElementByClassName("android.widget.EditText").sendKeys("busyQA");
		//find element when you have duplicate elements with same name
		driver.findElementsByClassName("android.widget.Button").get(1).click();
	}
}
