package brokenLinks;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class VerifyLinks {
	    WebDriver driver;

        @Test
	    public void brokenlinks() 
	    {
	
	    System.setProperty("webdriver.chrome.driver","C:\\Users\\Raju\\Documents\\Software\\WebDriver\\chromedriver.exe");
	    driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);  
	    driver.manage().window().maximize();
		driver.get("https://www.busyqa.com/");
		//driver.get("https://www.ww18.tollfree.cloud/");
	    //driver.get("https://www.amazon.ca/");
	    
		//list WebElements with tagName= 'a' and store that info into links object.
		List<WebElement> links=driver.findElements(By.tagName("a"));
		
		//print the size of WebElements stored into links object.
		System.out.println("Total links are "+links.size());
		
		//verify each link (from link zero to all links of the web site) by using for loop and verifyLinkactive() method. 
		for(int i=0;i<links.size();i++)
		{
			
		WebElement ele= links.get(i);
		String url=ele.getAttribute("href");
		verifyLinkActive(url);
		}	}
        
        //verifyLinkActive() Method.
	    public static void verifyLinkActive(String linkUrl)
	    {
        try
        {
        URL url = new URL(linkUrl);
        HttpURLConnection httpURLConnect=(HttpURLConnection)url.openConnection();
        httpURLConnect.setConnectTimeout(3000);
        httpURLConnect.connect();
        if(httpURLConnect.getResponseCode()==200) {
        System.out.println(linkUrl+" - "+httpURLConnect.getResponseMessage());
        }
        if(httpURLConnect.getResponseCode()==HttpURLConnection.HTTP_NOT_FOUND)
        {
        System.out.println(linkUrl+" - "+httpURLConnect.getResponseMessage() + " - "+ HttpURLConnection.HTTP_NOT_FOUND);
        }
        } 
        catch (Exception e) {
        }
        }
	
        }
