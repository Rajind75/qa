package Week4;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class androidUIautomatortestcase extends base {

	public static void main(String[] args) throws MalformedURLException {
		AndroidDriver<AndroidElement> driver = capabilities();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		//just write ("attribute(\"value|\")"), no tag name required.
		driver.findElementByAndroidUIAutomator("text(\"Views\")").click();
		//how to validate clickable feature for all options.
		//driver.findElementsByAndroidUIAutomator("new UiSelector().property(value)");
		System.out.println(driver.findElementByAndroidUIAutomator("new UiSelector().property(value)").getSize());
		
	}

}
