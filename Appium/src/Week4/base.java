package Week4;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class base {

			public static AndroidDriver<AndroidElement> capabilities() throws MalformedURLException {
			File f = new File("src");
			File fs = new File(f,"ApiDemos-debug.apk");
			DesiredCapabilities cap = new DesiredCapabilities(); //connect with the server
			cap.setCapability(MobileCapabilityType.DEVICE_NAME, "DemoNexus5x"); // connect with the device on which test has to be done
			cap.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath()); //connect path of the App.
			URL url = new URL("http://127.0.0.1:4723/wd/hub");
			AndroidDriver<AndroidElement> driver = new AndroidDriver<>(url,cap); //setting url and connect device & path parameters.
			return driver;
		}


}
